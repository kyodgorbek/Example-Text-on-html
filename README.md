# Example-Text-on-html
<DOCTYPE html>
<head>
          <title>Text</title>
	  <style type="text/css">
	  body {
	     padding:   20px;}
	  h1, h2, h3, a {
	   font-weight: normal;
	   color: #00088dd;
	   margin:   opx;}
	h1 {
	   font-family: Georgia, Times, serif;
	   font-size: 250%;
	     text-shadow: 2px 2px  3px #66666;
	     padding-bottom: 10px;}
	h2 {
	    font-family: "Gil Sans", Arial, sans-serif;
	    font-size: 90%;
	    text-transform: uppercase;
	    letter-spacing: 0.2.em;}
	h3 {
	   font-size: 150%;}
	p {
	 font-family: Arial, Verdana, sans-serif;
	 line-height: 1.4em;
	 color:   #665544;}
	p.intro:first-line {
	   font-weight: bold;}
	.credits
	   font-style: italic;
	   text-align: right;}
	a {
	  text-decoration: none;}
	a:hover {
	    text-docoration: undeline;}
	</style>
      </head>
  <body> 
	    <h1>Briards</h1>
	    <h2>A Heart wrapped in fur</h2>
	 <p class="intro">The <a class="breed" href="http://en.wikipedia.org/wikiBriard">
	     briard</a>, or berger de brie, is a large breed of dog traditionaly as a herder
	     and gurdian of sheep.</p>
	  <h3>Breed History</h3>
	    <p>The briard, Which is believed to have originated in France, has been bred for
		    centuries to herd and to protect sheep.The breed was used by the French Army as
	            sentrues, messengers and to search for wounded soldiers because of its fine sense
		    of hearing. Briards were used in the First World War almost to the point of
		    extinction. Currently the population of briards is slowly recovering.
		    Charlemagned, Napeleon,Thomas Jefferson and Lafyette all owned briards.</p>
		<p class="credits">by Ivy Duck</p>
</body>
</html>
